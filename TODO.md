## Features

* Timing-driven placement: integrate timing-driven placement with a callback
* Routing-driven placement: integrate routing-driven placement with a callback

## Ease-of-development

* More randomization:
    * Add noise to the global placement parameters based on the seed
    * Make the detailed placer less deterministic

## Performance

* Optimization for various quality/time tradeoffs
* Parallel implementation for detailed placement
* Legalization cost models other than L1
* Reoptimization of legalization
