"""
Coloquinte VLSI placer
"""


import gzip
import lzma
import os
import sys

import coloquinte_pybind
from coloquinte_pybind import (
    CellOrientation,
    DetailedPlacerParameters,
    GlobalPlacerParameters,
    LegalizationModel,
    NetModel,
    Rectangle,
)


def _open_file(name, write=False):
    """
    Open the file with the appropriate decompression method. In read mode, search for compressed versions if the exact name does not exist.
    """
    mode = "wt" if write else "rt"
    if name.endswith(".gz"):
        return gzip.open(name, mode=mode)
    elif name.endswith(".xz") or name.endswith(".lzma"):
        return lzma.open(name, mode=mode)
    elif write:
        return open(name, mode=mode)
    elif os.path.exists(name):
        return open(name, mode=mode)
    elif os.path.exists(name + ".gz"):
        return gzip.open(name + ".gz", mode=mode)
    elif os.path.exists(name + ".xz"):
        return lzma.open(name + ".xz", mode=mode)
    elif os.path.exists(name + ".lzma"):
        return lzma.open(name + ".lzma", mode=mode)
    else:
        raise RuntimeError(f"Could not find file {name}")


def _read_aux(filename):
    if os.path.isdir(filename):
        dir_list = os.listdir(filename)
        all_files = [f for f in dir_list if f.endswith(".aux")]
        default_name = os.path.basename(os.path.normpath(filename)) + ".aux"
        if len(all_files) == 1:
            filename = os.path.join(filename, all_files[0])
        elif len(all_files) > 1 and default_name in all_files:
            filename = os.path.join(filename, default_name)
        else:
            raise RuntimeError(
                f"There should be one file ending with .aux, got {', '.join(all_files)}"
            )
    elif not os.path.exists(filename):
        filename = filename + ".aux"
    dirname = os.path.dirname(filename)
    files = []
    with open(filename) as f:
        for line in f:
            for name in line.split():
                files.append(name)
    node_files = [n for n in files if n.endswith(".nodes")]
    net_files = [n for n in files if n.endswith(".nets")]
    pl_files = [n for n in files if n.endswith(".pl")]
    scl_files = [n for n in files if n.endswith(".scl")]
    if len(node_files) != 1:
        raise RuntimeError("There should be a .nodes file in .aux")
    if len(net_files) != 1:
        raise RuntimeError("There should be a .nets file in .aux")
    if len(pl_files) != 1:
        raise RuntimeError("There should be a .pl file in .aux")
    if len(scl_files) != 1:
        raise RuntimeError("There should be a .scl file in .aux")
    return (
        filename,
        os.path.join(dirname, node_files[0]),
        os.path.join(dirname, net_files[0]),
        os.path.join(dirname, pl_files[0]),
        os.path.join(dirname, scl_files[0]),
    )


def _parse_num_line(line):
    tokens = line.split(":")
    if len(tokens) != 2:
        raise RuntimeError(f"Couldn't interpret <{line}> as <Key : Value>")
    return int(tokens[1].strip())


def _read_nodes(filename):
    nb_nodes = None
    nb_terminals = None
    nodes = []
    with _open_file(filename) as f:
        first_line_found = False
        for line in f:
            line = line.strip()
            if len(line) == 0:
                continue
            if line.startswith("#"):
                continue
            if line.startswith("UCLA") and not first_line_found:
                first_line_found = True
                continue
            if line.startswith("NumNodes"):
                assert nb_nodes is None
                nb_nodes = _parse_num_line(line)
                continue
            if line.startswith("NumTerminals"):
                assert nb_terminals is None
                nb_terminals = _parse_num_line(line)
                continue
            vals = line.split()
            assert 3 <= len(vals) <= 4
            name, width, height = vals[:3]
            width = int(width)
            height = int(height)
            fixed = False
            if len(vals) == 4:
                assert vals[3] == "terminal"
                fixed = True
            nodes.append((name, width, height, fixed))
    if nb_nodes is not None:
        assert len(nodes) == nb_nodes
    if nb_terminals is not None:
        assert len([n for n in nodes if n[3]]) == nb_terminals
    names = [n[0] for n in nodes]
    widths = [n[1] for n in nodes]
    heights = [n[2] for n in nodes]
    fixed = [n[3] for n in nodes]
    return names, widths, heights, fixed


def _read_nets(filename, cell_names, cell_widths, cell_heights, sort_entries=False):
    name_dir = dict((name, i) for i, name in enumerate(cell_names))
    nb_nets = None
    nb_pins = None
    net_degree = None
    nets = []
    with _open_file(filename) as f:
        first_line_found = False
        for line in f:
            line = line.strip()
            if len(line) == 0:
                continue
            if line.startswith("#"):
                continue
            if line.startswith("UCLA") and not first_line_found:
                first_line_found = True
                continue
            if line.startswith("NumNets"):
                assert nb_nets is None
                nb_nets = _parse_num_line(line)
                continue
            if line.startswith("NumPins"):
                assert nb_pins is None
                nb_pins = _parse_num_line(line)
                continue
            line = line.replace(":", " ")
            if line.startswith("NetDegree"):
                vals = line.split()
                assert 2 <= len(vals) <= 3
                net_degree = int(vals[1])
                if len(vals) == 3:
                    name = vals[2]
                else:
                    name = f"n{len(nets)}"
                nets.append((name, net_degree, []))
                continue
            vals = line.split()
            assert len(vals) == 4 or len(vals) == 2
            if len(vals) == 4:
                cell, direction, x, y = vals
                x = float(x)
                y = float(y)
            else:
                cell, direction = vals
                x = 0.0
                y = 0.0
            assert cell in name_dir
            nets[-1][2].append((name_dir[cell], direction, x, y))
    total_pins = 0
    for name, net_degree, pins in nets:
        if net_degree != len(pins):
            raise RuntimeError(
                f"Net degree for {name} is {len(pins)}; expected {net_degree}"
            )
        total_pins += net_degree
    if nb_nets is not None:
        assert len(nets) == nb_nets
    if nb_pins is not None:
        assert total_pins == nb_pins
    if sort_entries:
        # Sort so that same-size nets are contiguous
        nets.sort(key=lambda net: len(net[-1]))
    cell_x_offset = [0.5 * c for c in cell_widths]
    cell_y_offset = [0.5 * c for c in cell_heights]
    ret = []
    for name, _, pins in nets:
        cells = []
        pin_x = []
        pin_y = []
        for cell, _, x, y in pins:
            cells.append(cell)
            pin_x.append(int(round(cell_x_offset[cell] + x)))
            pin_y.append(int(round(cell_y_offset[cell] + y)))
        ret.append((name, cells, pin_x, pin_y))
    return ret


def _read_place(filename, cell_names):
    name_dir = dict((name, i) for i, name in enumerate(cell_names))
    cell_x = [0 for i in cell_names]
    cell_y = [0 for i in cell_names]
    cell_orient = [None for i in cell_names]

    with _open_file(filename) as f:
        first_line_found = False
        for line in f:
            line = line.strip()
            if len(line) == 0:
                continue
            if line.startswith("#"):
                continue
            if line.startswith("UCLA") and not first_line_found:
                first_line_found = True
                continue
            line = line.replace(":", " ")
            vals = line.split()
            assert len(vals) >= 3
            cell, x, y, orient = vals[:4]
            assert cell in name_dir
            cell_ind = name_dir[cell]
            cell_x[cell_ind] = int(x)
            cell_y[cell_ind] = int(y)
            if orient in CellOrientation.__members__:
                cell_orient[cell_ind] = CellOrientation.__members__[orient]
            else:
                raise RuntimeError(f"Unknown orientation encountered {orient}")
    return cell_x, cell_y, cell_orient


def _read_rows(filename):
    nb_rows = None
    rows = []
    with _open_file(filename) as f:
        lines = [l.strip() for l in f]
        for line in lines:
            if line.startswith("NumRows"):
                assert nb_rows is None
                nb_rows = _parse_num_line(line)
        row_descs = []
        in_row = False
        for line in lines:
            if line.startswith("CoreRow"):
                row_descs.append([])
                in_row = True
            elif line.startswith("End"):
                in_row = False
            elif in_row:
                row_descs[-1].extend(line.replace(":", " ").split())
        for desc in row_descs:
            min_x = None
            min_y = None
            width = None
            height = None
            for i in range(1, len(desc)):
                if desc[i - 1].lower() == "coordinate":
                    min_y = int(desc[i])
                if desc[i - 1].lower() == "subroworigin":
                    min_x = int(desc[i])
                if desc[i - 1].lower() == "numsites":
                    width = int(desc[i])
                if desc[i - 1].lower() == "height":
                    height = int(desc[i])

            assert min_x is not None
            assert min_y is not None
            assert width is not None
            assert height is not None
            rows.append((min_x, min_x + width, min_y, min_y + height))
        return rows


class Circuit(coloquinte_pybind.Circuit):
    def __init__(self, nb_cells):
        super(Circuit, self).__init__(nb_cells)
        self._filename = None
        self._cell_name = None
        self._net_name = None

    @staticmethod
    def read_ispd(filename, ignore_obstructions=False):
        """
        Read an ISPD benchmark from its .aux file
        """
        (
            aux_filename,
            node_filename,
            net_filename,
            pl_filename,
            scl_filename,
        ) = _read_aux(filename)
        cell_names, cell_widths, cell_heights, cell_is_fixed = _read_nodes(
            node_filename
        )
        nets = _read_nets(net_filename, cell_names, cell_widths, cell_heights)
        cell_x, cell_y, cell_orient = _read_place(pl_filename, cell_names)
        rows = _read_rows(scl_filename)

        ret = Circuit(len(cell_names))
        ret._filename = os.path.splitext(aux_filename)[0]

        # Setup cell properties
        ret._cell_name = cell_names
        ret.cell_width = cell_widths
        ret.cell_height = cell_heights
        ret.cell_is_fixed = cell_is_fixed
        if ignore_obstructions:
            # All fixed cells marked as not obstructions
            ret.cell_is_obstruction = [not f for f in cell_is_fixed]

        # Setup nets and pins
        ret._net_name = []
        for name, cells, pins_x, pins_y in nets:
            ret._net_name.append(name)
            ret.add_net(cells, pins_x, pins_y)

        # Setup initial cell placement
        ret.cell_x = cell_x
        ret.cell_y = cell_y
        ret.cell_orientation = cell_orient

        # Setup rows
        ret.rows = [coloquinte_pybind.Rectangle(*row) for row in rows]
        ret.check()
        return ret

    def place_global(self, params, callback=None):
        """
        Run the global placement
        """
        if not isinstance(params, GlobalPlacerParameters):
            if not isinstance(params, int):
                raise TypeError("Argument should be an integer effort")
            params = GlobalPlacerParameters(params)
        super().place_global(params, callback)

    def legalize(self, params, callback=None):
        """
        Run the legalization
        """
        if not isinstance(params, DetailedPlacerParameters):
            if not isinstance(params, int):
                raise TypeError("Argument should be an integer effort")
            params = DetailedPlacerParameters(params)
        super().legalize(params, callback)

    def place_detailed(self, params, callback=None):
        """
        Run the detailed placement
        """
        if not isinstance(params, DetailedPlacerParameters):
            if not isinstance(params, int):
                raise TypeError("Argument should be an integer effort")
            params = DetailedPlacerParameters(params)
        super().place_detailed(params, callback)

    def load_placement(self, filename):
        if filename is None:
            return

        cell_x, cell_y, cell_orient = _read_place(filename, self._cell_name)
        self.cell_x = cell_x
        self.cell_y = cell_y
        self.cell_orientation = cell_orient

    def write_placement(self, filename):
        """
        Write the placement result in ISPD file format
        """
        if filename is None:
            if self._filename is None:
                raise RuntimeError("No filename to export placement to")
            filename = os.path.splitext(self._filename)[0] + ".sol.pl"
        with _open_file(filename, True) as f:
            print("UCLA pl 1.0", file=f)
            print("# Created by Coloquinte", file=f)
            print("# https://github.com/Coloquinte/PlaceRoute", file=f)
            print("", file=f)
            cell_x = self.cell_x
            cell_y = self.cell_y
            cell_orientation = self.cell_orientation
            cell_is_fixed = self.cell_is_fixed
            for i in range(self.nb_cells):
                name = self._cell_name[i]
                x = cell_x[i]
                y = cell_y[i]
                orient = cell_orientation[i].name
                if cell_is_fixed[i]:
                    orient += " /FIXED"
                print(f"{name}\t{x}\t{y}\t: {orient}", file=f)

    def write_image(self, filename, macros_only=False, image_width=2048):
        img = self._draw_cells(macros_only)
        self._save_image(img, filename, image_width)

    def write_displacement(self, filename, pl1, pl2, image_width=2048):
        from PIL import ImageDraw
        img = self._draw_cells(True)
        draw = ImageDraw.Draw(img)
        fixed = self.cell_is_fixed
        assert len(pl1) == len(pl2)
        for i in range(self.nb_cells):
            if not fixed[i]:
                x1 = (pl1[i].min_x + pl1[i].max_x) // 2
                x2 = (pl2[i].min_x + pl2[i].max_x) // 2
                y1 = (pl1[i].min_y + pl1[i].max_y) // 2
                y2 = (pl2[i].min_y + pl2[i].max_y) // 2
                draw.line([(x1, y1), (x2, y2)], fill="red", width=2)
                draw.arc([x1 - 1, y1 - 1, x1 + 1, y1 + 1],
                         0, 360, fill="black")
        self._save_image(img, filename, image_width)

    def _save_image(self, img, filename, image_width):
        from PIL import Image
        new_height = int(image_width * img.height / img.width)
        img = img.resize((image_width, new_height), Image.LANCZOS)
        img.save(filename)

    def _draw_cells(self, macros_only):
        from PIL import Image, ImageDraw

        placement = self.cell_placement
        fixed = self.cell_is_fixed

        min_x = min(pl.min_x for pl in placement)
        min_y = min(pl.min_y for pl in placement)
        max_x = max(pl.max_x for pl in placement)
        max_y = max(pl.max_y for pl in placement)
        img = Image.new("RGB", (max_x - min_x, max_y - min_y), (255, 255, 255))
        draw = ImageDraw.Draw(img)
        for i, pl in enumerate(placement):
            if fixed[i]:
                draw.rectangle(
                    [(pl.min_x, pl.min_y), (pl.max_x, pl.max_y)],
                    fill="gray",
                    outline="black",
                    width=8,
                )
            elif not macros_only:
                draw.rectangle(
                    [(pl.min_x, pl.min_y), (pl.max_x, pl.max_y)], fill="blue"
                )
        return img


def _add_arguments(parser, obj, prefix):
    for name in obj.__dir__():
        if name.startswith("_"):
            continue
        if name in ["check", "seed"]:
            continue
        arg_type = type(getattr(obj, name))
        if arg_type in (int, float):
            parser.add_argument(
                "--" + prefix + "." + name,
                type=arg_type,
                metavar=name.upper(),
            )
        else:
            parser.add_argument(
                "--" + prefix + "." + name,
                choices=list(arg_type.__members__.keys()),
                metavar=name.upper(),
            )


def _parse_arguments(args, obj, prefix):
    for name in obj.__dir__():
        if name.startswith("_"):
            continue
        if name in ["check", "seed"]:
            continue
        val = getattr(args, prefix + "." + name)
        if val is not None:
            new_val = val
            old_val = getattr(obj, name)
            arg_type = type(old_val)
            if arg_type not in (int, float):
                val = arg_type.__members__[val]
                old_val = old_val.name
            print(
                f"Overloading {prefix} placement parameter {name} "
                f"({old_val} -> {new_val})"
            )
            setattr(obj, name, val)


def _show_params(obj, prefix):
    for name in obj.__dir__():
        if name.startswith("_"):
            continue
        if name in ["check", "seed"]:
            continue
        default_val = getattr(obj, name)
        arg_type = type(default_val)
        if arg_type not in (int, float):
            default_val = default_val.name
        print(f"\t--{prefix}.{name}: {default_val}")


class WriteImagesCallback:
    def __init__(self, circuit, prefix, image_width, extension):
        self.circuit = circuit
        self.step = 1
        self.prefix = prefix
        self.image_width = image_width
        self.extension = extension

    def __call__(self, step_name):
        filename = f"{self.prefix}_{self.step:04d}_{step_name.name.lower()}.{self.extension}"
        self.circuit.write_image(filename, image_width=self.image_width)
        self.step += 1


def main():
    """
    Run the whole placement algorithm from the command line
    """
    import argparse

    parser = argparse.ArgumentParser(
        description="Place a benchmark circuit from the command line",
        usage="usage: coloquinte [-h] [--effort EFFORT] [--seed SEED] [--load-solution FILE] [--save-solution FILE] instance"
    )
    parser.add_argument("instance", help="Benchmark instance")
    parser.add_argument("--effort", help="Placement effort",
                        type=int, default=3)
    parser.add_argument("--seed", help="Random seed", type=int, default=-1)
    parser.add_argument(
        "--load-solution", help="Load initial placement", metavar="FILE"
    )
    parser.add_argument("--save-solution",
                        help="Save final placement", metavar="FILE")
    parser.add_argument(
        "--show-parameters", help="Show parameter values", action="store_true"
    )
    group = parser.add_mutually_exclusive_group()
    group.add_argument(
        "--no-global", help="Skip global placement", action="store_true")
    group.add_argument(
        "--only-global",
        help="Run only global placement (no legalization)",
        action="store_true",
    )
    group.add_argument(
        "--no-detailed", help="Skip detailed placement", action="store_true"
    )
    parser.add_argument(
        "--ignore-macros",
        help="Ignore fixed placement obstructions",
        action="store_true",
        dest="ignore_obstructions",
    )

    # Prefix to save images
    parser.add_argument("--save-images", help=argparse.SUPPRESS, type=str)
    # Save intermediate placement images
    parser.add_argument("--save-all-images",
                        help=argparse.SUPPRESS, action="store_true")
    # Save intermediate placement images
    parser.add_argument(
        "--image-width", help=argparse.SUPPRESS, type=int, default=1080)
    # Save intermediate placement images
    parser.add_argument("--image-extension",
                        help=argparse.SUPPRESS, type=str, default="webp")

    global_group = parser.add_argument_group("Global placement parameters")
    detailed_group = parser.add_argument_group("Detailed placement parameters")
    _add_arguments(global_group, GlobalPlacerParameters(), "global")
    _add_arguments(detailed_group, DetailedPlacerParameters(), "detailed")
    args = parser.parse_args()

    global_params = GlobalPlacerParameters(args.effort, args.seed)
    _parse_arguments(args, global_params, "global")
    global_params.check()
    detailed_params = DetailedPlacerParameters(args.effort, args.seed)
    _parse_arguments(args, detailed_params, "detailed")
    detailed_params.check()

    if args.show_parameters:
        print("Parameter values:")
        _show_params(global_params, "global")
        _show_params(detailed_params, "detailed")

    circuit = Circuit.read_ispd(args.instance, args.ignore_obstructions)
    print(circuit.report())
    if args.ignore_obstructions:
        print("Ignoring macros for standard cell placement")
    if args.load_solution is not None:
        print(f"Loading initial solution")
        circuit.load_placement(args.load_solution)

    sys.stdout.flush()
    callback = None
    if args.save_images is not None:
        circuit.write_image(
            f"{args.save_images}_macros.{args.image_extension}", True, args.image_width)
        image_writer = WriteImagesCallback(
            circuit, args.save_images, args.image_width, args.image_extension)
        if args.save_all_images:
            callback = image_writer

    if args.no_global:
        print("Global placement skipped at user's request")
    else:
        circuit.place_global(global_params, callback)

    sys.stdout.flush()
    if args.only_global:
        print("Legalization and detailed placement skipped at user's request")
    elif args.no_detailed:
        circuit.legalize(detailed_params, callback)
        print("Detailed placement skipped at user's request")
    else:
        circuit.place_detailed(detailed_params, callback)
    if args.save_images is not None:
        circuit.write_image(
            f"{args.save_images}_placed.{args.image_extension}", True, args.image_width)
    circuit.write_placement(args.save_solution)


__all__ = [
    "Circuit",
    "GlobalPlacerParameters",
    "DetailedPlacerParameters",
    "Rectangle",
    "LegalizationModel",
    "NetModel",
    "CellOrientation",
    "main",
]

if __name__ == "__main__":
    main()
